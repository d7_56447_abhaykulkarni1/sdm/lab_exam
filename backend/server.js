// get the required dependencies
const db = require("./db");
const cors = require("cors");
const express = require("express");
const utils = require("./utils");

// create  node app
const app = express();

// add Json format and cross origin
app.use(express.json());
app.use(cors("*"));

//Display Book using the name from Containerized MySQL
app.get("/books/:name", (req, resp) => {
  // create statement
  const { name } = req.params;
  const sql = `select * from Book where book_title= '${name}'`;

  // execute the query
  db.execute(sql, (err, data) => {
    resp.send(utils.createResult(err, data));
  });
});

// ADD Book data into Containerized MySQL table
app.post("/books/add", (req, resp) => {
  // create statement
  const { book_title, publisher_name, author_name } = req.body;
  const sql = `Insert into Book (book_title,publisher_name,author_name) values ('${book_title}','${publisher_name}','${author_name}')`;

  // execute the query
  db.execute(sql, (err, data) => {
    resp.send(utils.createResult(err, data));
  });
});

app.delete("/books/delete", (req, resp) => {
  // create statement
  const { book_id } = req.body;
  const sql = `Delete from Book where book_id=${book_id}`;
  // execute the query
  db.execute(sql, (err, data) => {
    resp.send(utils.createResult(err, data));
  });
});

app.patch("/books/update/:book_id", (req, resp) => {
  // create statement
  const { book_id } = req.params;
  const { publisher_name, author_name } = req.body;
  const sql = `Update Book Set publisher_name = '${publisher_name}', author_name='${author_name}' where book_id= ${book_id}`;

  // execute the query
  db.execute(sql, (err, data) => {
    resp.send(utils.createResult(err, data));
  });
});

// start listing on port
app.listen(4001, "0.0.0.0", () => {
  console.log("Server started at port 4001");
});
