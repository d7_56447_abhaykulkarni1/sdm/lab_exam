create table Book ( book_id INT primary key auto_increment, book_title VARCHAR(200), publisher_name varchar(100), author_name varchar(100));

Insert into Book (book_title,publisher_name,author_name) values ("C programming","TataMcgrawhill","Yashvant Kanetkar");
Insert into Book (book_title,publisher_name,author_name) values ("C++ programming","TataMcgrawhill","Yashvant Kanetkar");
Insert into Book (book_title,publisher_name,author_name) values ("C++ programming","TataMcgrawhill","Yashvant Kanetkar");
